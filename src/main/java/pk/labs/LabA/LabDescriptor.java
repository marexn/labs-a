package pk.labs.LabA;

import pk.labs.LabA.Contracts.EkspressFunkcje;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = DisplayImpl.class.getName();
    public static String controlPanelImplClassName = ControlPanelImpl.class.getName();

    public static String mainComponentSpecClassName = EkspressFunkcje.class.getName();
    public static String mainComponentImplClassName = EkspresDoKawy.class.getName();
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "app";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly-frame";
    // endregion
}
